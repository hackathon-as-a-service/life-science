import './medicalReportView.scss';
import * as React from 'react';

/* Import: helpers */
import { observer } from 'mobx-react';
import { observable } from 'mobx';
import * as Cookies from 'es-cookie';

/* Import: components  */
import { Button, TextField } from '@material-ui/core';
import { UserModel, domain } from '../../model/rootModel';

interface MedicalReportViewStates {
    progress: string;
    ticket: string;
}

const Reports: {
    [type: string]: { [part: string]: { diagnose: string; image: string }[] };
} = {
    all: {
        head: [],
        arm: [],
        upper: [],
        lower: [],
        leg: []
    },
    adult: {
        head: [
            {
                diagnose: 'The patient feels pain in the headarea.',
                image: 'headache.png'
            }
        ],
        arm: [],
        upper: [
            {
                diagnose: 'The patient has pain in the upper body.',
                image: 'hearth.png'
            },
            {
                diagnose: 'The patient is coughing a lot.',
                image: 'coughing.png'
            }
        ],
        lower: [],
        leg: []
    },
    female: {
        head: [],
        arm: [],
        upper: [],
        lower: [],
        leg: []
    },
    male: {
        head: [],
        arm: [],
        upper: [],
        lower: [],
        leg: []
    },
    children: {
        head: [],
        arm: [],
        upper: [{
            diagnose: 'The patient has pain in the upper body.',
            image: 'hearth.png'
        }],
        lower: [],
        leg: []
    }
};

@observer
export class MedicalReportView extends React.Component<
    { user: UserModel },
    MedicalReportViewStates
> {
    private person: string;
    private part: string;
    private diagnoses: { [diagnose: string]: boolean } = {};

    extractDiagnoses(): string[] {
        const props = Object.getOwnPropertyNames(this.diagnoses);

        const digs: string[] = [];
        props.forEach(value => {
            if (this.diagnoses[value] && value !== '$mobx') digs.push(value);
        });

        return digs;
    }

    constructor(props: null) {
        super(props);

        this.state = {
            progress: '',
            ticket: ''
        };

        this.props.user.request().then(() => {
            const age = parseInt(
                new Number(
                    (new Date().getTime() -
                        this.props.user.birthday.getTime()) /
                        31536000000
                ).toFixed(0)
            );

            if (age < 12) this.person = 'children';
            else if (this.props.user.sex === 'female') this.person = 'female';
            else this.person = 'male';

            this.setState({
                progress: this.person
            });
        });
    }

    componentDidUpdate() {
        if (this.state.progress === 'ticket' && this.state.ticket === '') {
            let diag = this.props.user.pregnant
                ? 'The patient is pregnant. '
                : '';
            this.extractDiagnoses().forEach(value => {
                diag += value + ' ';
            });
            fetch(domain + '/patient/diseasereport', {
                method: 'POST',
                body: JSON.stringify({
                    diagnose: diag
                }),
                cache: 'no-cache',
                credentials: 'include',
                headers: {
                    'Content-Type': 'application/json',
                    'X-Content-Type-Options': 'nosniff',
                    'patient-token': this.props.user.token
                },
                mode: 'cors'
            })
                .then(res => {
                    return res.json();
                })
                .then(data => {
                    this.setState({
                        ticket: data.accessCode
                    });
                });
        }
    }

    render() {
        return (
            <div className={'medicalReport'}>
                {(() => {
                    switch (this.state.progress) {
                        case 'children': {
                            return (
                                <div
                                    className={'parts--children'}
                                    onClick={() => {
                                        this.setState({ progress: 'reports' });
                                    }}
                                >
                                    <img
                                        className={'parts--children__head'}
                                        src={'/images/children/head.png'}
                                        onClick={() => {
                                            this.part = 'head';
                                        }}
                                    />
                                    <img
                                        className={'parts--children__arm--left'}
                                        src={'/images/children/arm.png'}
                                        onClick={() => {
                                            this.part = 'arm';
                                        }}
                                    />
                                    <img
                                        className={
                                            'parts--children__arm--right'
                                        }
                                        src={'/images/children/arm.png'}
                                        onClick={() => {
                                            this.part = 'arm';
                                        }}
                                    />
                                    <img
                                        className={'parts--children__upper'}
                                        src={'/images/children/upper.png'}
                                        onClick={() => {
                                            this.part = 'upper';
                                        }}
                                    />
                                    <img
                                        className={
                                            'parts--children__leg--right'
                                        }
                                        src={'/images/children/leg.png'}
                                        onClick={() => {
                                            this.part = 'leg';
                                        }}
                                    />
                                    <img
                                        className={'parts--children__leg--left'}
                                        src={'/images/children/leg.png'}
                                        onClick={() => {
                                            this.part = 'leg';
                                        }}
                                    />
                                </div>
                            );
                        }
                        case 'female': {
                            return (
                                <div
                                    className={'parts--female'}
                                    onClick={() => {
                                        this.setState({ progress: 'reports' });
                                    }}
                                >
                                    <img
                                        className={'parts--female__head'}
                                        src={'/images/female/head.png'}
                                        onClick={() => {
                                            this.part = 'head';
                                        }}
                                    />
                                    <img
                                        className={'parts--female__arm--left'}
                                        src={'/images/female/arm.png'}
                                        onClick={() => {
                                            this.part = 'arm';
                                        }}
                                    />
                                    <img
                                        className={'parts--female__arm--right'}
                                        src={'/images/female/arm.png'}
                                        onClick={() => {
                                            this.part = 'arm';
                                        }}
                                    />
                                    <img
                                        className={'parts--female__upper'}
                                        src={'/images/female/upper.png'}
                                        onClick={() => {
                                            this.part = 'upper';
                                        }}
                                    />
                                    <img
                                        className={'parts--female__lower'}
                                        src={'/images/female/lower.png'}
                                        onClick={() => {
                                            this.part = 'lower';
                                        }}
                                    />
                                    <img
                                        className={'parts--female__leg--right'}
                                        src={'/images/female/leg.png'}
                                        onClick={() => {
                                            this.part = 'leg';
                                        }}
                                    />
                                    <img
                                        className={'parts--female__leg--left'}
                                        src={'/images/female/leg.png'}
                                        onClick={() => {
                                            this.part = 'leg';
                                        }}
                                    />
                                </div>
                            );
                        }
                        case 'male': {
                            return (
                                <div
                                    className={'parts--male'}
                                    onClick={() => {
                                        this.setState({ progress: 'reports' });
                                    }}
                                >
                                    <img
                                        className={'parts--male__head'}
                                        src={'/images/male/head.png'}
                                        onClick={() => {
                                            this.part = 'head';
                                        }}
                                    />
                                    <img
                                        className={'parts--male__arm--left'}
                                        src={'/images/male/arm.png'}
                                        onClick={() => {
                                            this.part = 'arm';
                                        }}
                                    />
                                    <img
                                        className={'parts--male__arm--right'}
                                        src={'/images/male/arm.png'}
                                        onClick={() => {
                                            this.part = 'arm';
                                        }}
                                    />
                                    <img
                                        className={'parts--male__upper'}
                                        src={'/images/male/upper.png'}
                                        onClick={() => {
                                            this.part = 'upper';
                                        }}
                                    />
                                    <img
                                        className={'parts--male__lower'}
                                        src={'/images/male/lower.png'}
                                        onClick={() => {
                                            this.part = 'lower';
                                        }}
                                    />
                                    <img
                                        className={'parts--male__leg--right'}
                                        src={'/images/male/leg.png'}
                                        onClick={() => {
                                            this.part = 'leg';
                                        }}
                                    />
                                    <img
                                        className={'parts--male__leg--left'}
                                        src={'/images/male/leg.png'}
                                        onClick={() => {
                                            this.part = 'leg';
                                        }}
                                    />
                                </div>
                            );
                        }
                        case 'reports': {
                            return (
                                <div className={'reports'}>
                                    {Reports[this.person][this.part].map(
                                        value => (
                                            <Button
                                                style={{
                                                    background: this.diagnoses[
                                                        value.diagnose
                                                    ]
                                                        ? 'red'
                                                        : ''
                                                }}
                                                className={'card'}
                                                key={this.person + value.image}
                                                variant={'contained'}
                                                color={'primary'}
                                                onClick={() => {
                                                    if (
                                                        !this.diagnoses[
                                                            value.diagnose
                                                        ]
                                                    )
                                                        this.diagnoses[
                                                            value.diagnose
                                                        ] = true;
                                                    else
                                                        this.diagnoses[
                                                            value.diagnose
                                                        ] = false;

                                                    this.setState({});
                                                }}
                                            >
                                                <img
                                                    src={
                                                        '/images/diagnose/' +
                                                        value.image
                                                    }
                                                />
                                            </Button>
                                        )
                                    )}{' '}
                                    {Reports['all'][this.part].map(value => (
                                        <Button
                                            style={{
                                                background: this.diagnoses[
                                                    value.diagnose
                                                ]
                                                    ? 'red'
                                                    : ''
                                            }}
                                            className={'card'}
                                            key={this.person + value.image}
                                            variant={'contained'}
                                            color={'primary'}
                                            onClick={() => {
                                                if (
                                                    !this.diagnoses[
                                                        value.diagnose
                                                    ]
                                                )
                                                    this.diagnoses[
                                                        value.diagnose
                                                    ] = true;
                                                else
                                                    this.diagnoses[
                                                        value.diagnose
                                                    ] = false;

                                                this.setState({});
                                            }}
                                        >
                                            <img
                                                src={
                                                    '/images/diagnose/' +
                                                    value.image
                                                }
                                            />
                                        </Button>
                                    ))}
                                    {this.person === 'male' ||
                                    this.person === 'female'
                                        ? Reports['adult'][this.part].map(
                                              value => (
                                                  <Button
                                                      style={{
                                                          background: this
                                                              .diagnoses[
                                                              value.diagnose
                                                          ]
                                                              ? 'red'
                                                              : ''
                                                      }}
                                                      className={'card'}
                                                      key={
                                                          this.person +
                                                          value.image
                                                      }
                                                      variant={'contained'}
                                                      color={'primary'}
                                                      onClick={() => {
                                                          if (
                                                              !this.diagnoses[
                                                                  value.diagnose
                                                              ]
                                                          )
                                                              this.diagnoses[
                                                                  value.diagnose
                                                              ] = true;
                                                          else
                                                              this.diagnoses[
                                                                  value.diagnose
                                                              ] = false;

                                                          this.setState({});
                                                      }}
                                                  >
                                                      <img
                                                          src={
                                                              '/images/diagnose/' +
                                                              value.image
                                                          }
                                                      />
                                                  </Button>
                                              )
                                          )
                                        : null}
                                    <Button
                                        key={'button--back'}
                                        className={'button--back'}
                                        variant={'contained'}
                                        color={'secondary'}
                                        onClick={() => {
                                            this.setState({
                                                progress: this.person
                                            });
                                        }}
                                    >
                                        <svg
                                            xmlnsXlink="http://www.w3.org/1999/xlink"
                                            version="1.1"
                                            viewBox="0 0 492 492"
                                            height={'30px'}
                                        >
                                            <path
                                                d="M464.344,207.418l0.768,0.168H135.888l103.496-103.724c5.068-5.064,7.848-11.924,7.848-19.124    c0-7.2-2.78-14.012-7.848-19.088L223.28,49.538c-5.064-5.064-11.812-7.864-19.008-7.864c-7.2,0-13.952,2.78-19.016,7.844    L7.844,226.914C2.76,231.998-0.02,238.77,0,245.974c-0.02,7.244,2.76,14.02,7.844,19.096l177.412,177.412    c5.064,5.06,11.812,7.844,19.016,7.844c7.196,0,13.944-2.788,19.008-7.844l16.104-16.112c5.068-5.056,7.848-11.808,7.848-19.008    c0-7.196-2.78-13.592-7.848-18.652L134.72,284.406h329.992c14.828,0,27.288-12.78,27.288-27.6v-22.788    C492,219.198,479.172,207.418,464.344,207.418z"
                                                fill="#FFFFFF"
                                            />
                                        </svg>
                                    </Button>
                                </div>
                            );
                        }
                        case 'ticket': {
                            return [
                                <svg
                                    version="1.1"
                                    style={{
                                        paddingTop: '-50px'
                                    }}
                                    key={'svg-ticket'}
                                    xmlnsXlink="http://www.w3.org/1999/xlink"
                                    viewBox="0 0 512.001 512.001"
                                    height={'300px'}
                                >
                                    <path
                                        fill="#A6D3E8"
                                        d="M120.606,41.274v68.994H53.043V41.274C53.043,22.623,68.167,7.5,86.832,7.5
	c9.326,0,17.769,3.788,23.885,9.89C116.833,23.505,120.606,31.949,120.606,41.274z"
                                    />
                                    <path
                                        fill="#D0F0FE"
                                        d="M449.067,17.39c-6.116-6.101-14.559-9.89-23.885-9.89H86.832c9.326,0,17.769,3.788,23.885,9.89
	c6.116,6.116,9.889,14.559,9.889,23.885V504.5l33.832''-28.916l33.847,28.916l33.847-28.916l33.832,28.916l33.832-28.916
	l33.846,28.916l33.832-28.916l33.832,28.916l33.818-28.916l33.832,28.916V110.269V41.274
	C458.957,31.949,455.183,23.505,449.067,17.39z"
                                    />
                                    <path
                                        fill="#A6D3E8"
                                        d="M449.067,17.389c-6.116-6.101-14.559-9.889-23.885-9.889h-24.736c9.326,0,17.769,3.788,23.885,9.889
	c6.116,6.116,9.889,14.559,9.889,23.885v68.994v373.09l24.736,21.142V110.269V41.274C458.957,31.949,455.183,23.505,449.067,17.389z
	"
                                    />
                                    <circle
                                        fill="#FFFFFF"
                                        cx="289.78"
                                        cy="110.27"
                                        r="70.796"
                                    />
                                    <polygon
                                        fill="#E36F91"
                                        points="301.206,98.844 301.206,73.014 278.357,73.014 278.357,98.844 252.527,98.844 
	252.527,121.693 278.357,121.693 278.357,147.523 301.206,147.523 301.206,121.693 327.036,121.693 327.036,98.844 "
                                    />
                                    <path
''="M454.371,12.086c-0.002-0.002-0.004-0.004-0.006-0.006C446.557,4.29,436.193,0,425.183,0H86.832
''v68.995c0,4.142,3.358,7.5,7.5,7.5h36.254c4.142,0,7.5-3.358,7.5-7.5s-3.358-7.5-7.5-7.5
''5,86.832,15c7.01,0,13.611,2.734,18.582,7.693c4.961,4.96,7.693,11.559,7.693,18.581V504.5
''8,1.224,5.786,0.792,8.012-1.11l28.96-24.752l28.974,24.753c2.806,2.397,6.938,2.397,9.743,0
''98,6.939,2.398,9.746,0l28.96-24.752l28.974,24.753c2.806,2.398,6.939,2.397,9.745-0.001
''399,6.942,2.398,9.747-0.001l28.945-24.749l28.958,24.75c1.385,1.184,3.121,1.799,4.875,1.799
''9-1.225,4.361-3.884,4.361-6.812V41.274C466.457,30.246,462.165,19.88,454.371,12.086z
''07-2.398-6.941-2.398-9.747,0.001l-28.945,24.75l-28.958-24.751
''.96,24.753l-28.974-24.754c-2.806-2.397-6.939-2.397-9.745,0.001l-28.959,24.751
''399-9.745-0.001l-28.975,24.754l-28.975-24.754c-1.403-1.199-3.137-1.798-4.872-1.798
	s-3.47,0.6-4.873,1.799l-21.459,18.341V41.274c0-0.651-0.018-1.299-0.047-1.945c-0.01-0.212-0.028-0.423-0.041-0.635
	c-0.026-0.433-0.055-0.866-0.095-1.296c-0.023-0.249-0.053-0.496-0.08-0.744c-0.043-0.393-0.09-0.786-0.144-1.176
	c-0.036-0.257-0.076-0.513-0.117-0.77c-0.06-0.38-0.125-0.758-0.196-1.134c-0.048-0.257-0.098-0.513-0.151-0.768
	c-0.079-0.379-0.164-0.756-0.254-1.132c-0.058-0.245-0.115-0.49-0.177-0.733c-0.102-0.398-0.214-0.793-0.328-1.187
	c-0.062-0.215-0.12-0.432-0.186-0.646c-0.15-0.488-0.312-0.972-0.48-1.454c-0.04-0.116-0.076-0.233-0.117-0.348
	c-0.213-0.595-0.441-1.185-0.682-1.769c-0.068-0.164-0.142-0.324-0.212-0.487c-0.179-0.418-0.36-0.836-0.553-1.248
	c-0.1-0.214-0.207-0.425-0.311-0.638c-0.175-0.357-0.351-0.713-0.537-1.065c-0.118-0.225-0.242-0.448-0.364-0.672
	c-0.185-0.336-0.373-0.669-0.567-1c-0.131-0.224-0.265-0.446-0.4-0.668c-0.201-0.327-0.407-0.651-0.617-0.973
	c-0.14-0.215-0.28-0.43-0.424-0.642c-0.225-0.331-0.457-0.657-0.692-0.981c-0.141-0.194-0.278-0.39-0.422-0.583
	c-0.277-0.369-0.564-0.732-0.854-1.093c-0.112-0.139-0.219-0.282-0.333-0.42c-0.018-0.022-0.034-0.044-0.052-0.066h306.51
	c7.01,0,13.611,2.734,18.587,7.699c4.957,4.959,7.686,11.556,7.686,18.575v446.949H451.457z"
                                    />
                                    <path
                                        d="M395.434,395.04h-96.495c-4.142,0-7.5,3.358-7.5,7.5s3.358,7.5,7.5,7.5h96.495c4.142,0,7.5-3.358,7.5-7.5
	S399.576,395.04,395.434,395.04z"
                                    />
                                    <path
                                        d="M395.434,343.13h-96.495c-4.142,0-7.5,3.358-7.5,7.5s3.358,7.5,7.5,7.5h96.495c4.142,0,7.5-3.358,7.5-7.5
	S399.576,343.13,395.434,343.13z"
                                    />
                                    <path
                                        d="M405.019,267.532H174.545c-4.142,0-7.5,3.358-7.5,7.5s3.358,7.5,7.5,7.5h230.474c4.142,0,7.5-3.358,7.5-7.5
	S409.161,267.532,405.019,267.532z"
                                    />
                                    <path
                                        d="M174.545,235.548h115.237c4.142,0,7.5-3.358,7.5-7.5s-3.358-7.5-7.5-7.5H174.545c-4.142,0-7.5,3.358-7.5,7.5
	S170.402,235.548,174.545,235.548z"
                                    />
                                    <path
                                        d="M289.781,188.565c43.172,0,78.296-35.123,78.296-78.296c0-15.689-4.625-30.828-13.376-43.781
	c-2.319-3.432-6.98-4.333-10.414-2.016c-3.432,2.319-4.335,6.981-2.016,10.414c7.069,10.463,10.806,22.699,10.806,35.384
	c0,34.901-28.395,63.296-63.296,63.296s-63.296-28.394-63.296-63.296c0-34.901,28.394-63.296,63.296-63.296
	c10.538,0,20.968,2.64,30.162,7.633c3.642,1.979,8.194,0.629,10.17-3.01c1.977-3.64,0.629-8.193-3.01-10.17
	c-11.385-6.184-24.29-9.453-37.322-9.453c-43.172,0-78.296,35.124-78.296,78.296C211.486,153.441,246.609,188.565,289.781,188.565z"
                                    />
                                    <path
                                        d="M308.706,91.344v-18.33c0-4.142-3.358-7.5-7.5-7.5h-22.849c-4.142,0-7.5,3.358-7.5,7.5v18.33h-18.33
	c-4.142,0-7.5,3.358-7.5,7.5v22.849c0,4.142,3.358,7.5,7.5,7.5h18.33v18.33c0,4.142,3.358,7.5,7.5,7.5h22.849
	c4.142,0,7.5-3.358,7.5-7.5v-18.33h18.33c4.142,0,7.5-3.358,7.5-7.5V98.844c0-4.142-3.358-7.5-7.5-7.5H308.706z M319.536,114.193
	h-18.33c-4.142,0-7.5,3.358-7.5,7.5v18.33h-7.849v-18.33c0-4.142-3.358-7.5-7.5-7.5h-18.33v-7.849h18.33c4.142,0,7.5-3.358,7.5-7.5
	v-18.33h7.849v18.33c0,4.142,3.358,7.5,7.5,7.5h18.33V114.193z"
                                    />
                                </svg>,
                                <div className={'preview'}>
                                    <u>Tomorrow</u>
                                    <br />
                                    Uniklinikum MA
                                </div>,
                                <input
                                    style={{
                                        width: 'min-content',
                                        userSelect: 'none',
                                        marginTop: '50px'
                                    }}
                                    key={'input-ticket'}
                                    className={'input-ticket'}
                                    value={this.state.ticket}
                                />
                            ];
                        }
                        default: {
                            return [
                                <div key="main" className="main" />,
                                <div key="pic" className="pic" />
                            ];
                        }
                    }
                })()}
                {this.state.progress !== 'token' &&
                this.state.progress !== 'ticket' ? (
                    <Button
                        key={'button--next'}
                        disabled={this.extractDiagnoses().length === 0}
                        className={'button--next'}
                        variant={'contained'}
                        color={'secondary'}
                        onClick={() => {
                            this.setState({
                                progress: 'ticket'
                            });
                        }}
                    >
                        <svg
                            style={{ transform: 'scaleX(-1)' }}
                            xmlnsXlink="http://www.w3.org/1999/xlink"
                            version="1.1"
                            viewBox="0 0 492 492"
                            height={'30px'}
                        >
                            <path
                                d="M464.344,207.418l0.768,0.168H135.888l103.496-103.724c5.068-5.064,7.848-11.924,7.848-19.124    c0-7.2-2.78-14.012-7.848-19.088L223.28,49.538c-5.064-5.064-11.812-7.864-19.008-7.864c-7.2,0-13.952,2.78-19.016,7.844    L7.844,226.914C2.76,231.998-0.02,238.77,0,245.974c-0.02,7.244,2.76,14.02,7.844,19.096l177.412,177.412    c5.064,5.06,11.812,7.844,19.016,7.844c7.196,0,13.944-2.788,19.008-7.844l16.104-16.112c5.068-5.056,7.848-11.808,7.848-19.008    c0-7.196-2.78-13.592-7.848-18.652L134.72,284.406h329.992c14.828,0,27.288-12.78,27.288-27.6v-22.788    C492,219.198,479.172,207.418,464.344,207.418z"
                                fill="#FFFFFF"
                            />
                        </svg>
                    </Button>
                ) : null}
            </div>
        );
    }
}
