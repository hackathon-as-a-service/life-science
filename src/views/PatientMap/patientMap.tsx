import "./patientView.scss";

import * as React from "react";
import { Map, TileLayer, Marker, Popup } from "react-leaflet";

/* Import: helpers */
import { observer } from "mobx-react";
import { Zoom } from "@material-ui/core";

interface PatientData {
    location: {
        lat: number;
        lang: number;
    };
    birthday: string;
    diseaseReport: {
        diagnose: string;
    };
    isPregnant: boolean;
}

interface Location {
    lat: number;
    lang: number;
}

export class PatientMap extends React.Component<
    {},
    {
        lat: number;
        lng: number;
        zoom: number;
        markers: PatientData[];
        locations: Location[];
    }
> {
    constructor(props: null) {
        super(props);

        this.state = {
            lat: 51.505,
            lng: -0.09,
            zoom: 13,
            markers: [],
            locations: []
        };
    }

    componentDidMount() {
        this.fetchMapData();
    }

    componentDidUpdate() {
        setTimeout(() => this.fetchMapData(), 5000);
    }

    fetchMapData() {
        fetch("/api/v1/patient/map?minLat=0&minLang=0&maxLang=190&maxLat=190", {
            method: "GET",
            cache: "no-cache",
            credentials: "include",
            headers: {
                "Content-Type": "application/json",
                "X-Content-Type-Options": "nosniff"
            },
            mode: "cors"
        })
            .then((res: any) => {
                return res.json();
            })
            .then((json: any) => {
                this.setState({
                    markers: json
                });
            });

        /*fetch("/api/v1/ngo/map", {
            method: "GET",
            cache: "no-cache",
            credentials: "include",
            headers: {
                "Content-Type": "application/json",
                "X-Content-Type-Options": "nosniff"
            },
            mode: "cors"
        })
            .then((res: any) => {
                return res.json();
            })
            .then((json: any) => {
                console.log(json);
                this.setState({
                    locations: json
                });
            });*/
    }

    render() {
        return (
            <Map
                center={[this.state.lat, this.state.lng]}
                zoom={this.state.zoom}
                id="map"
            >
                <TileLayer
                    attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
                    url="http://{s}.tile.osm.org/{z}/{x}/{y}.png"
                />
                {this.state.markers.map((data, index) => (
                    <Marker
                        position={[data.location.lat, data.location.lang]}
                        key={index}
                    >
                        <Popup>
                            {"Pregnant:" + data.isPregnant} <br />
                            {"Age:" + data.birthday}
                            <br />
                            <br />
                            Report: <br /> {data.diseaseReport.diagnose}
                        </Popup>
                    </Marker>
                ))}
            </Map>
        );
    }
}
